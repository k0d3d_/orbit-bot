# Serverless Instagram Bot using Docker and Azure Functions.
I’ll write about how I learnt to create the serverless component of an Instagram Bot using Azure Functions, Cosmos DB and Machine Learning using TensorFlow.js . The bot will like the first 10 photos on a timeline and also post a comment or reply comments and post photos. I am hoping this will help any one reading this to build apps that automate social media interaction like these ones on the Android Play Store. At least build the backend of the app.

## In technical summary, I will —

   * Use Azure Functions to handle requests which will determine what automated actions the bot will have to take.
   * Deploy Browserless to an Azure Container Instance.
   * Using Browserless to execute headless browser operations all inside of Docker containers.
   * Azure Functions Bindings and Triggers.
   * Deploy Azure functions using Continuous Integration for faster deployments using git. (coming soon)
   * Secure data transfer of credentials and Browserless access.
   * Use TensorFlow.js to detect HTML elements properly.
